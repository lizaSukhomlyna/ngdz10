package com.dz10;

public interface CoordinateScanner {
    Coordinate scan();
}